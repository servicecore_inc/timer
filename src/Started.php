<?php

namespace ServiceCore\Timer;

class Started extends Timer
{
    /**
     * @var  float  the timer's start time as a unix timestamp with microseconds
     */  
    private $start;

    public function __construct(float $start = null)
    {
        $this->start = $start ?: \microtime(true);
    }

    public function getStart(): float
    {
        return $this->start;
    }

    public function diff(): float
    {
        return \microtime(true) - $this->start;
    }

    public function stop(): Stopped
    {
        return new Stopped($this->start, \microtime(true));
    }
}
