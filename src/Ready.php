<?php

namespace ServiceCore\Timer;

class Ready extends Timer
{
    public function start(): Started
    {
        return new Started(microtime(true));
    }
}
