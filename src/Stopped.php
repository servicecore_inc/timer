<?php

namespace ServiceCore\Timer;

class Stopped extends Timer
{
    /**
     * @var  float  the timer's start time as a unix timestamp with microseconds
     */  
    private $start;

    /**
     * @var  float  the timer's stop time as a unix timestamp with microseconds
     */
    private $stop;

    public function __construct(float $start, float $stop)
    {
        $this->start = $start;
        $this->stop  = $stop;
    }

    public function getStart(): float
    {
        return $this->start;
    }

    public function getStop(): float
    {
        return $this->stop;
    }

    public function diff(): float
    {
        return $this->stop - $this->start;
    }

    public function reset(): Ready
    {
        return new Ready();
    }
}
