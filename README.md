# Timer
A simple PHP timer.

```
$timer = new Ready();

$timer = $timer->start();

// ... do something that takes 1.23456789 seconds

$timer = $timer->stop();

echo $timer->diff(); 
```

The example above would output:

```
1.23456789
```

There are three states to the timer: _ready_, _started_, and _stopped_.

## Version

### 0.1.0, March 3, 2016

* Initial release
