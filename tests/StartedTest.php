<?php

namespace ServiceCore\Timer\Test;

use ServiceCore\Timer\Started;
use ServiceCore\Timer\Stopped;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class StartedTest extends TestCase
{
    public function testConstructSetsStartIfStartDoesNotExist(): void
    {
        $now   = \microtime(true);
        $timer = new Started();
        $class = new ReflectionClass($timer);

        $property = $class->getProperty('start');
        $property->setAccessible(true);

        $this->assertGreaterThanOrEqual($now, $property->getValue($timer));
    }

    public function testConstructSetsStartIfStartDoesExist(): void
    {
        $start = \microtime(true);
        $timer = new Started();
        $class = new ReflectionClass($timer);

        $property = $class->getProperty('start');
        $property->setAccessible(true);

        $this->assertEquals(\round($start), \round($property->getValue($timer)));
    }

    public function testGetStartReturnsFloat(): void
    {
        $start = \microtime(true);
        $timer = new Started();
        $class = new ReflectionClass($timer);

        $property = $class->getProperty('start');
        $property->setAccessible(true);

        $this->assertEquals(\round($start), \round($timer->getStart()));
    }

    public function testDiffReturnsFloat(): void
    {
        $start = \microtime(true);
        $timer = new Started($start);

        $this->assertGreaterThanOrEqual(\microtime(true) - $start, $timer->diff());
    }

    public function testStopReturnsTimer(): void
    {
        $this->assertInstanceOf(Stopped::class, (new Started())->stop());
    }
}
