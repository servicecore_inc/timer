<?php

namespace ServiceCore\Timer\Test;

use ServiceCore\Timer\Ready;
use ServiceCore\Timer\Started;
use PHPUnit\Framework\TestCase;

class ReadyTest extends TestCase
{
    public function testStartReturnsTimer(): void
    {
        $this->assertInstanceOf(Started::class, (new Ready())->start());
    }
}
