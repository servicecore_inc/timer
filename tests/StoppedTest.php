<?php

namespace ServiceCore\Timer\Test;

use ServiceCore\Timer\Ready;
use ServiceCore\Timer\Stopped;
use PHPUnit\Framework\TestCase;

class StoppedTest extends TestCase
{
    public function testConstruct(): void
    {
        $startValue = \microtime(true);
        $stopValue  = $startValue + 60;
        $timer      = new Stopped($startValue, $stopValue);
        $class      = new \ReflectionClass($timer);

        $startProperty = $class->getProperty('start');
        $startProperty->setAccessible(true);

        $stopProperty = $class->getProperty('stop');
        $stopProperty->setAccessible(true);

        $this->assertEquals($startValue, $startProperty->getValue($timer));
        $this->assertEquals($stopValue, $stopProperty->getValue($timer));
    }

    public function testGetStart(): void
    {
        $start = \microtime(true);
        $timer = new Stopped(\microtime(true), \microtime(true));
        $class = new \ReflectionClass($timer);

        $property = $class->getProperty('start');
        $property->setAccessible(true);
        $property->setValue($timer, $start);

        $this->assertEquals($start, $timer->getStart());
    }

    public function testGetStop(): void
    {
        $stop  = \microtime(true);
        $timer = new Stopped(\microtime(true), \microtime(true));
        $class = new \ReflectionClass($timer);

        $property = $class->getProperty('stop');
        $property->setAccessible(true);
        $property->setValue($timer, $stop);

        $this->assertEquals($stop, $timer->getStop());
    }

    public function testResetReturnsTimer(): void
    {
        $timer = new Stopped(\microtime(true), \microtime(true));

        $this->assertInstanceOf(Ready::class, $timer->reset());
    }
}
